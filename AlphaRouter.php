<?php
namespace Alpha\Router;

use Exception;
use phpDocumentor\Reflection\DocBlock\Tags\Example;

/*
*Router Class
*/

class AlphaRouter {
  /*
   *  Apply the traits.
   */
  use
    \Alpha\Router\Traits\Configuration;

  private $_initialized = false;
  /****************DEBUG PROPERTIES**************/
  private $_callstack = [];
  private $_debug     = 0;
  /****************END DEBUG PROPERTIES**********/
  private $_error = [];

  // Flag the specifies if the routing table was loaded from the cache.
  private $_fromCache = false;

  // Executed Handler functions.
  private $_calledHandlers = [];

  // Executed Middleware functions.
  private $_calledMiddleware = [];

  // Resolved URLs.
  private $_history = [];

  // Loaded Route files.
  private $_loadedRouteFiles = [];
  private $_filename = null;

  // Default route file pattern used to match files.
  private $_pattern = '/(.*)route\.php$/i';

  //the root mount point for the url.
  private $_mountPoint = null;

  //Flag that specifies if the request has been complete.
  private $_resolved = false;

  // Instance of the middleware component.
  public $middleware = null;

  // Instance of the FileLoader.
  public $loader = null;

  public $router = null;

  // Instance of the hook componenet.
  public $hook = null;

  public function __construct($config = null) {

    //Setup Defalt configuration for the router.
    $this->_config = [
      // Flag that determines if script execution is terminated on a successful handler call.
      'exitonsucess' => true,
      'cache.filename' => 'router',
      'cache' => false
    ];

    $this->configure($config);

    // Instantiate the middleware component
    $this->middleware = new \Alpha\Router\Middleware\Middleware();
    // Instantiate the hook component
    $this->hook = new \Alpha\Router\Hook\Hook();
    // Instantiate the FileLoader.
    $this->loader = new \Alpha\Router\FileLoader\FileLoader();

    // Instantiate the router.
    $this->router = new \Alpha\Router\Routing\Router();
  }

  public function __call($name, $args) {
  }

  public function done() {
    return $this->_resolved;
  }

  public function getCalledHandlers() {
    return $this->_calledHandlers;
  }

  public function getCalledMiddleware() {
    return $this->_calledMiddleware;
  }

  public function mount($val) {
    if($this->_debug) {
      $this->_called(__FUNCTION__, func_get_args());
    }
    $this->_mountPoint = $val;

    return $this;
  }

  public function nodeCount($pattern) {
    $trimmed = trim($pattern, '/');
    return empty($trimmed) ? 0 : count(explode('/', $trimmed));
  }

  public function __toString() {
    if($this->_debug) {
      $this->_called(__FUNCTION__, func_get_args());
    }
    return serialize($this);
  }

  public function __destruct() {
    //check if caching and store the list.
    if(is_object($cache = $this->getConfig('cache')) && !$this->_fromCache) {
      if(method_exists($cache, 'set')) {

        //Check if Cashing and write to the cache.
        $cache->set($this->getConfig('cache.filename'), $this->router->export());
      }
    }
  }

  public function get($url, $handler) {
    return $this->setRoute('get', $url, $handler);
  }

  public function post($url, $handler) {
    return $this->setRoute('post', $url, $handler);
  }

  public function delete($url, $handler) {
    return $this->setRoute('delete', $url, $handler);
  }

  public function put($url, $handler) {
    return $this->setRoute('put', $url, $handler);
  }

  public function fallback($handler) {
    if(is_callable($handler)) {
      $this->router->fallback($handler);
    }
    return $this;
  }

  //Match Any of the verbs to the same handler method.
  public function match(Array $methods, $url, $handler) {
    $ct = count($methods);
    if($ct === 0) {
      //todo - property handle the fact that bad data was supplied.
      return $this;
    }
    for($i = 0; $i < $ct; ++$i) {
      $this->setRoute(trim($methods[ $i ]), $url, $handler);
    }

    return $this;
  }

  public function getRoutingTable() {
    return $this->router->getRoutingTable();
  }
  /*
  * ---- Private methods ----
  */
  private function init() {

    // If cache exists
    if(is_object($cache = $this->getConfig('cache'))) {
      if(method_exists($cache, 'get')) {

        //Check if Cashing and write to the cache.
        if(!empty($routes = $cache->get($this->getConfig('cache.filename')))) {

          // Route import callback - Will resolve each route's filename.
          $onRoute = function($route, $router) {
            $filename = $route->meta('filename');

            // Load the route file.
            $this->bulk($filename);

          };
          //var_dump($routes);
          $this->router->import($routes, $onRoute);
          // Return to to prevent routes from being reloaded.
          return true;
        }
      }
    }

    // else reload the route files.
    $this->loader->match(
      $this->getConfig('routes'),
      $this->_pattern,
      function($filepath) {
        $this->bulk($filepath);
      }
    );
  }

  public function bulk($filepath) {

    $this->_filename = $filepath;

    $routes = $this->loader->load($filepath);
    //Check that a function is provided in the '$route' variable.
    if(is_callable($routes)) {
      // Call any 'before' Hooks.
      if($hook = $this->hook->get('pre', 'loadfile')) {
        $hook($routes);
      } else {
        $routes($this);
      }

      //reset the mount point that may have been set by the route file
      $this->_mountPoint = null;

      //The routefile is require by the loadRoute method to properly build a cache.
      $this->_filename = null;

      return true;
    }
  }

  private function getRoute($method, $url) {
    return $this->router->getRoute($method, $url);
  }

  public function route($method, $url) {

    if(!isset($method) || empty($method)) {
      throw new \Exception("Request METHOD required.");
    }

    if(!isset($url) || empty($url)) {
      throw new \Exception("Request URL required.");
    }

    // Initialize the Routing Table;
    $this->init();

    // Save the requested raw url.
    $this->_history[] = $url;

    // Get the route data from the routing table.
    $route = $this->getRoute($method, $url);

    //check if a handler method has been assigned.
    if(!is_null($route)) {
      return $this->handleRequest($route);
    }

    //If no matches have been found, return a 404 error code.
    return false;
  }

  private function setRoute($method, $url, $handler, $meta = []) {
    if(is_string($this->_mountPoint) && !empty($this->_mountPoint)) {
      $url = $url === '/' ? $this->_mountPoint : $this->_mountPoint . $url;
    }
    $meta = is_array($meta) ? $meta : [];

    // Add the filename to the meta object if exists.
    $meta['filename'] = $this->_filename;

    $this->router->addRoute($method, $url, $handler, $meta);
    return $this;
  }

  /*
   * @method executeMiddleware()
   * @param String $pattern - Regex pattern for the uri.
   * @param Array $params - Array of route parameters.
   * @return Array - Returns an Array items returned from each function call.
   */
  private function executeMiddleware($pattern, $params) {

    $args = [];

    $middleware = $this->middleware->reduce($pattern);

    // Execute the 'before' hooks.
    $preHook = $this->hook->get('pre', 'middleware');
    $postHook = $this->hook->get('post', 'middleware');

    if($mCount = count($middleware)) {
      for($i = 0; $i < $mCount; ++$i) {
        $fn = $middleware[$i];
        // Execute the 'before' hooks.
        if(is_callable($preHook)) {
          $args[] = $preHook($fn ,$params, $args);
        } else {
          $args[] = $fn($params, $args);
        }

        // Save a reference to the executed middleware function
        $this->_calledMiddleware[] = $fn;

        // Execute the 'after' hooks.
        if(is_callable($postHook)) {
          $args[] = $postHook($fn,$params, $args);
        }
      }
    }
    return $args;
  }

  private function handleRequest($route) {
    $handler = $route->getHandler();
    $params = $route->params();
    $pattern = $route->pattern();
    //Check if the request has already been handled.
    if($this->_resolved === true) { return $this; }

    $args = [];

    // Execute the middleware for the current URL.
    $args = $this->executeMiddleware($this->_history[count($this->_history)-1], $params);

    //Check if the '$handler' is an array of callbacks.
    if(is_array($handler)) {
      //call the handlers in a sequence.
      $count = count($handler);
      for($i = 0; $i < $count; ++$i) {
        $args[] = $this->resolve($handler[ $i ], $params, $args);
      }
    } else {
      $this->resolve($handler, $params, $args);
    }

    /*
     * Set the resolved flag to true if all of the handlers
     * have been called.
     */
    $this->_resolved = true;

    /*
     * Request has been handled, terminate the script
     * unless exitonsucess flag was set to false.
     */
    if($this->getConfig('exitonsucess') === false) {
      return $this;
    }
    exit(0);
  }

  private function resolve($handler, $params = null, $args = null) {
    if(is_callable($handler)) {
      //Results from the handler.
      $result = null;

      // Check if a pre resolve hook is set, and call that passing in the handler.
      if($hook = $this->hook->get('pre', 'resolve')){
        $result = $hook($handler, $params, $args);
      } else {
        $result = $handler($this, $params, $args);
      }

      // Save a reference to the called handler.
      $this->_calledHandlers[] = $handler;

      //Check for an after resolve hook.
      if($hook = $this->hook->get('post', 'resolve')){
        $result = $hook($result);
      }

      return $result;
    } else {
      throw new \Exception('The supplied handler method is not callable');
    }
  }
}
