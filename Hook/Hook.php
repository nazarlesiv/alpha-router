<?php
namespace Alpha\Router\Hook;

class Hook {
  // pre and post Hooks for certain methods.
  private $_hooks = [
    'pre' => [],
    'post' => []
  ];

  public function pre($event, $handler) {
    $this->_hooks['pre'][$event] = $handler;
  }

  public function post($event, $handler) {
    $this->_hooks['post'][$event] = $handler;
  }

  public function get($condition, $caller) {
    // Convert to lowercase.
    $condition = strtolower($condition);
    $caller = strtolower($caller);

    // If Exists return the function.
    return isset($this->_hooks[$condition][$caller]) &&
           is_callable($this->_hooks[$condition][$caller]) ?
           $this->_hooks[$condition][$caller] :
           null;
  }
}
