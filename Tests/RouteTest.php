<?php
// require_once('../Routing/Route.php');
use PHPUnit\Framework\TestCase;
use Alpha\Router\Routing\Route;

class RouteTest extends TestCase {
  public static function setUpBeforeClass(): void {}

  protected $Route = null;
  protected function setUp(): void {
    $this->Route = new Route();
  }

  protected function tearDown(): void
  {
    // Reset the custom route Parser if one was added.
    Route::setParser(null);
  }

  public function test_should_exist() {
    $this->assertNotNull(new Route(), 'Route object should be created.');
  }

  public function test_constructor_arguments() {
    $handler = function() {};
    $method = 'get';
    $uri = '/test/:id';
    $pattern = '/test/(\w+)';

    $route =  new Route($method, $uri, $handler);

    $this->assertEquals($method, $route->getMethod());
    $this->assertEquals([$handler], $route->getHandler(), 'Should return an array with handler function as first element');
    $this->assertEquals($uri, $route->getUri());
    $this->assertEquals($pattern, $route->pattern());
  }

  public function test_metadata() {
    $route =  new Route();
    $this->assertNull($route->meta('key'));
    $route->meta('key', 'value');
    $this->assertEquals('value', $route->meta('key'));
    $route->meta('array', ['array']);
    $this->assertEquals(['array'], $route->meta('array'));
    // Get all meta
    $this->assertEquals(['key' => 'value', 'array' => ['array']], $route->meta());
  }

  public function test_parameter_names() {
    $handler = function() {};
    $method = 'get';
    $uri = '/test/:id/another/:another';

    $route =  new Route($method, $uri, $handler);
    $this->assertEquals([':id', ':another'], $route->paramNames());
  }

  public function test_parameter_values() {
    $handler = function() {};
    $method = 'get';
    $uri = '/test/:id/another/:another';

    $route =  new Route($method, $uri, $handler);
    $this->assertTrue($route->isMatch('/test/1234/another/5678'), 'Should return true');
    $this->assertEquals(['1234', '5678'], $route->paramValues());
  }

  public function test_parameter_key_value_pairs() {
    $handler = function() {};
    $method = 'get';
    $uri = '/test/:id/another/:another';

    $route =  new Route($method, $uri, $handler);
    $this->assertTrue($route->isMatch('/test/1234/another/5678'), 'Should return true.');
    $this->assertEquals([':id', ':another'], $route->paramNames(), 'Should return route parameter names.');
    $this->assertEquals(['1234', '5678'], $route->paramValues(), 'should return route parameter values.');
    $this->assertEquals(['id' => '1234', 'another' => '5678'], $route->params(), 'Should build a paramameter map.');
  }

  public function test_resolve_route_with_params() {

    $method = 'get';
    $uri = '/test/:id/another/:another';

    $handler = function($routeParams) {
      $this->assertEquals(['id' => '1234', 'another' => '5678'], $routeParams, 'Should pass route parameters to handler.');
      return true;
    };

    $route =  new Route($method, $uri, $handler);
    $this->assertTrue($route->isMatch('/test/1234/another/5678'), 'Should return true.');
    $this->assertEquals([':id', ':another'], $route->paramNames(), 'Should return route parameter names.');
    $this->assertEquals(['1234', '5678'], $route->paramValues(), 'should return route parameter values.');
    $this->assertEquals(['id' => '1234', 'another' => '5678'], $route->params(), 'Should build a paramameter map.');

    $result = $route->resolve();

    $this->assertEquals([true], $result, 'Should return an array of handler return values.');

    $this->assertTrue($route->resolved());
  }

  public function test_node_count() {
    $handler = function() {};
    $method = 'get';

    $route =  new Route($method, '/test/:id/another/:another', $handler);
    $this->assertCount(4, $route->nodes(), 'Should accurately count nodes.');

    $route =  new Route($method, '/test/:id/another/:another/', $handler);
    $this->assertCount(4, $route->nodes(), 'Should accurately count nodes.');

    $route =  new Route($method, '/', $handler);
    $this->assertCount(0, $route->nodes(), 'Should accurately count nodes.');

    $route =  new Route($method, '/test', $handler);
    $this->assertCount(1, $route->nodes(), 'Should accurately count nodes.');
  }

  public function test_default_parser() {
    $handler = function() {};
    $method = 'get';
    $uri = '/test/:id';
    $pattern = '/test/(\w+)';

    $route =  new Route($method, $uri, $handler);

    $this->assertEquals($pattern,$route->pattern(), 'Should return the correct pattern.');
  }

  /**
   * @group parser
   */
  public function test_custom_parser() {
    $handler = function() {};
    $method = 'get';
    $uri = '/test/:id';
    $pattern = '/test/replacement';

    Route::setParser(function($uri, &$matches) {
      return preg_replace_callback('/(:\w+)/',
        function($captured) use (&$matches) {
          $matches[] = $captured[ 0 ];
          return 'replacement';
        }, $uri);
    });
    $route =  new Route($method, $uri, $handler);
    $this->assertEquals($pattern,$route->pattern(), 'Should return the correct pattern.');
    $this->assertEquals([':id'], $route->paramNames(), 'Should capture parameters');
  }

  /**
   * @group
   */
  public function test_serialize() {
    $handler = function() {};
    $method = 'get';
    $uri = '/test/:id';
    $pattern = '/test/(\w+)';

    $route =  new Route($method, $uri, $handler);

    // Add meta.
    $route->meta('filename', '/path/to/filename');

    $serialized = serialize($route);

    $this->assertTrue(is_string($serialized), 'Should successfully serialize Route object');

    $unserialized = unserialize($serialized);
    $this->assertEquals('Alpha\Router\Routing\Route', get_class($unserialized), 'Should unserialize to the correct object');

    // Validate the property values.
    $this->assertEquals($method, $unserialized->getMethod(), 'Should Return the correct method.');
    $this->assertEquals($uri, $unserialized->getUri(), 'Should Return the correct URI.');
    $this->assertEquals($pattern, $unserialized->pattern(), 'Should Return the correct pattern');
    $this->assertNull($unserialized->getHandler(), 'Should not return handler');
  }
}
