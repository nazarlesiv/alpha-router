<?php

use PHPUnit\Framework\TestCase;
use Alpha\Router\Routing\Router;

class RouterTest extends TestCase
{
  protected $Router = null;

  public static function setUpBeforeClass(): void
  {
  }

  protected function setUp(): void
  {
    //$this->Router = $this->App->router();
    $this->Router = new Router();

    // Default Metadata array.
    $this->meta = [
      'filename' => '/path/to/filename',
      'permissions' => ['read', 'write', 'execute']
    ];
  }

  public function test_should_exist()
  {
    $this->assertNotNull($this->Router, 'Router object should be created.');
  }

  public function test_add_route()
  {
    $router = $this->Router;

    $handler = function () {
    };
    $router->addRoute('get', '/', $handler);
    $this->assertEquals([$handler], $router->getRoute('get', '/')->getHandler());
  }

  public function test_get_route()
  {
    $router = $this->Router;

    $handler = function () {
    };
    $router->addRoute('get', '/:value', $handler, $this->meta);
    $this->assertEquals([$handler], $router->getRoute('get', '/value')->getHandler());
  }


  public function test_get_unregistered_route()
  {
    $router = $this->Router;

    $handler = function () {
    };
    $router->addRoute('get', '/test', $handler, $this->meta);
    $this->assertEquals(null, $router->getRoute('get', '/value'));
  }

  public function test_get_unregistered_root_route()
  {
    $router = $this->Router;

    $handler = function () {
    };
    $router->addRoute('get', '/test', $handler, $this->meta);
    // Try to get an unregistered root route
    $this->assertEquals(null, $router->getRoute('get', '/'));
  }

  public function test_get_empty_request()
  {
    $router = $this->Router;

    $handler = function () {
    };
    $router->addRoute('get', '/test', $handler, $this->meta);
    $this->assertEquals(null, $router->getRoute('', ''));
  }

  public function test_get_route_with_query()
  {
    $router = $this->Router;

    $handler = function () {
    };
    $router->addRoute('get', '/:value', $handler, $this->meta);
    $this->assertEquals([$handler], $router->getRoute('get', '/value?query=query')->getHandler());
  }

  public function test_get_route_from_empty_table()
  {
    $router = $this->Router;
    $this->assertNull($router->getRoute('get', '/value'));
  }


  /**
   * @group
   * Tests the api edit form
   */
  public function test_add_fallback_route()
  {
    $router = $this->Router;

    $handler = function () {
    };
    $router->fallback($handler, $this->meta);
    $this->assertEquals([$handler], $router->getRoute('get', '/')->getHandler());
    $this->assertEquals([$handler], $router->getRoute('get', '/test')->getHandler());
    $this->assertEquals([$handler], $router->getRoute('post', '/test1')->getHandler());
  }

  /**
   *@group fallback
   */
  public function test_add_method_specific_fallback()
  {
    $router = $this->Router;

    $handler = function () {
    };
    // Fallback Route
    $router->post('*', $handler, $this->meta);

    // Specific url
    $router->post('/test2', $handler);

    $this->assertNull($router->getRoute('get', '/test'));
    $this->assertEquals([$handler], $router->getRoute('post', '/test')->getHandler());
    $this->assertEquals([$handler], $router->getRoute('post', '/test2')->getHandler());
  }

  public function test_add_array_of_handlers()
  {
    $router = $this->Router;

    $handlers = [
      function () {
      },
      function () {
      }
    ];
    $router->addRoute('get', '/', $handlers);
    $this->assertEquals($handlers, $router->getRoute('get', '/')->getHandler());
  }


  // This is more of an integration test.
  public function test_add_route_with_metadata()
  {
    $router = $this->Router;

    $handlers = [
      function () {
      },
      function () {
      }
    ];

    $router->addRoute('get', '/', $handlers, $this->meta);

    $route = $router->getRoute('get', '/');
    $this->assertEquals($handlers, $route->getHandler(), 'Should return all handler methods');
    $this->assertEquals($this->meta, $route->meta(), 'Should Return all metadata');
  }

  public function test_use_exact_match()
  {
    $router = $this->Router;

    $exact = function () {
    };
    $handler = function () {
    };
    $router->addRoute('get', '/get/all', $exact);
    $router->addRoute('get', '/get/:id', $handler);

    $route = $router->getRoute('get', '/get/all');
    $this->assertEquals([$exact], $route->getHandler(), 'Should get Route with exact text match.');
  }

  /**
   *@group
   */
  public function test_export_routing_table()
  {
    $router = $this->Router;

    $router->addRoute('get', '/', function () {
    }, $this->meta)
      ->addRoute('post', '/test', function () {
      }, $this->meta);

    // Export will serialize all none-resource data.
    // Handlers will will not be serialized.
    // Custom Parser will not be serialized.
    $data = $router->export();

    $this->assertTrue(is_string($data), 'Should export data as a string');
  }

  /**
   *@group failing
   */
  public function test_custom_import_handler()
  {
    $router = $this->Router;

    // Import Handlers
    $fn = function ($route, $router) {
      $routes = include($route->meta('filename'));

      $routes($router);
    };

    $definition = include('fixtures/sample.route.php');
    $definition($router);
    $route = $router->getRoute('get', '/test/file');
    // Set the meta of filename.
    $route->meta('filename', 'fixtures/sample.route.php');

    // Assert that Route was properly loaded.
    $this->assertFalse(is_null($route), 'Should return route');

    // Export the routing table.
    $data = $router->export();

    // Create a new Router
    $newRouter = new Router();
    $newRouter->import($data, $fn);

    // Get the route and resolve it.
    $route = $newRouter->getRoute('get', '/test/file');

    $this->assertFalse(is_null($route), 'Should return route');
    $result = $route->resolve();
    $this->assertEquals(['testfile'], $result, 'Should Call the proper handler and capture result');

    // Get the route with query parameters.

    // Create a new Router
    $newRouter2 = new Router();
    $newRouter2->import($data, $fn);

    $route = $newRouter2->getRoute('get', '/test/file?query=test');
    $this->assertFalse(is_null($route), 'Should return route');
    $result = $route->resolve();
    $this->assertEquals(['testfile'], $result, 'Should Call the proper handler and capture result');
  }
}
