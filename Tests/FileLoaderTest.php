<?php
use PHPUnit\Framework\TestCase;
use Alpha\Router\FileLoader\FileLoader;


class FileLoaderTest extends TestCase {

  protected $Loader = null;
  protected $path = __DIR__ .'/fixtures';
  protected $pattern = '/(.*)route\.php$/i';

  public static function setUpBeforeClass(): void {}

  protected function setUp(): void {
    $this->Loader = new FileLoader();
  }

  public function test_should_exist() {
    $this->assertNotNull($this->Loader, 'Loader object should be created.');
  }

  public function test_reduce_files() {
    $files = $this->Loader->match($this->path, $this->pattern);
    $this->assertCount(3, $files, 'Should find all route files in all sub-directories');
  }

  public function test_reduce_with_callback() {
    $count = 0;
    $callback = function($filepath) use (&$count) { $count++; };

    $files = $this->Loader->match($this->path, $this->pattern, $callback);
    $this->assertCount(3, $files, 'Should find all route files in all sub-directories');
    $this->assertEquals(3, $count, 'Should call the callback function on each match.');
  }

  public function test_require() {
    $files = $this->Loader->match($this->path, $this->pattern);
    $this->assertCount(3, $files, 'Should find all route files in all sub-directories');

    $route = $this->Loader->load(array_pop($files));
    $this->assertTrue(!is_null($route), 'Should return a callable object');
  }
}
