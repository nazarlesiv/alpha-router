<?php
use PHPUnit\Framework\TestCase;
use Alpha\Router\Routing\Store;

class StoreTest extends TestCase {
  protected $store = null;

  public static function setUpBeforeClass(): void {}

  protected function setUp(): void {
    $this->store = new Store();
  }

  public function test_should_create_store_instance() {
    $this->assertNotNull($this->store, 'Store object should be created.');
  }

  public function test_should_set_tokenizer() {
    $fn = function($path) {
      return explode('/', trim($path, '/')); 
    };

    $this->store->setTokenizer($fn);
    $this->assertNotNull($this->store->getTokenizer(), 'Should get tokenizer');
  }

  public function test_should_tokenize_using_default() {
    $path = '/one/two/three';
    $nodes = $this->store->tokenize($path);

    $this->assertEquals($nodes, ['one', 'two', 'three'], 'Should tokenize using default tokenizer');
  }

  public function test_should_tokenize_using_custom_tokenizer() {
    $path = 'one.two.tree';
    $fn = function($path) {
      return explode('.', trim($path, '.')); 
    };

    $this->store->setTokenizer($fn);
    $this->assertEquals($this->store->tokenize($path), ['one', 'two', 'tree'], 'Should tokenize using custom tokenizer');
  }

  public function test_should_insert_value() {
    $paths = [
      '/one',
      '/one/two',
      '/one/two/three',
      '/one/two/three/four',
      '/four/one/two',
      'four/three/two/one'
    ];
    
    foreach($paths as $path) {
      $this->store->insert($path, $path); 
    }

    foreach($paths as $path) {
      $this->assertEquals($this->store->find($path), $path, "Should find Paths");
    }
  }
}
