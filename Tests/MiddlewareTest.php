<?php
use PHPUnit\Framework\TestCase;
use Alpha\Router\Middleware\Middleware;

class MiddlewareTest extends TestCase {
  protected $Middleware = null;

  public static function setUpBeforeClass(): void {}

  protected function setUp(): void {
    //$this->Router = $this->App->router();
    $this->Middleware = new Middleware();
  }

  public function test_should_exist() {
    $this->assertNotNull($this->Middleware, 'Middleware object should be created.');
  }

  public function test_add_middleware() {
    $middleware = $this->Middleware;
    $middleware
      ->add(function($args) {})
      ->add('/', function($args) {})
      ->add('/account', function($args) {})
      ->add('/account/all', function($args) {})
      ->add('/account/:id/users', function($args) {})
      ->add('/account/:id/users/get/all', function($args) {})
      ->add('/users/all', function($args) {})
      ->add('/users/remove/all', function($args) {});

      // Get all middleware.
      $this->assertCount(8, $middleware->reduce(), 'Correct number of Middleware should be added to the router');
      $this->assertCount(2, $middleware->reduce('/'), 'Should retrieve all middleware attached to root');
      $this->assertCount(3, $middleware->reduce('/account'), 'Should retrieve all middleware upto and including the node');
      $this->assertCount(4, $middleware->reduce('/account/all'));
      $this->assertCount(3, $middleware->reduce('/account/1234'));
      $this->assertCount(4, $middleware->reduce('/account/1234/users'));
      $this->assertCount(4, $middleware->reduce('/account/1234/users/get'));
      $this->assertCount(5, $middleware->reduce('/account/1234/users/get/all'));
      $this->assertCount(3, $middleware->reduce('/users/all'));
      $this->assertCount(3, $middleware->reduce('/users/remove/all'));
      $this->assertCount(2, $middleware->reduce('/users/remove'));
  }
}
