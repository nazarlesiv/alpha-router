<?php
use PHPUnit\Framework\TestCase;
use Alpha\Router\Routing\Node;

class NodeTest extends TestCase {

  public static function setUpBeforeClass(): void {}

  protected function setUp(): void {
    
  }

  public function test_should_create_root_node() {
    $node = new Node(null);
    $this->assertNotNull($node, 'Node object should be created.');
    $this->assertTrue($node->isRoot(), "IsRoot() should return true");
    $this->assertNull($node->getId(), "Root Node ID Should be Null");
  }

  public function test_should_create_node_with_id() {
    $id = '1';
    $node = new Node($id);
    $this->assertEquals($node->getId(), $id, "Should get node ID");
  }

  public function test_should_set_node_value() {
    $id = '1';
    $value = "test";
    $node = new Node($id);
    $node->setValue($value);

    $this->assertEquals($node->getId(), $id, "Should get node ID");
    $this->assertEquals($node->getValue(), $value, "Should get node value");
  }

  public function test_should_create_child_node() {
    $path = ['first', 'second'];

    $root = new Node(null);
    $node = $root->create($path);
    $this->assertNotNull($node, 'Node object should be created.');
    $this->assertEquals($node->getId(), $path[count($path) - 1], "New Node ID should be the last node in the path");

  }

  public function test_should_find_created_node() {
    $paths = [
      ['*', '*'],
      ['test', 'one'],
      ['test', 'two'],
      ['test', 'two', 'tree'],
      ['test', 'two', 'tree', 'four'],
      ['test', 'four', 'tree', 'two']
    ];
    
    $root = new Node(null);

    foreach($paths as $path) {
      $value = implode('/', $path);
      $node = $root->create($path)->setValue($value);

      $this->assertNotNull($node, 'Node object should be created.');
      $this->assertEquals($node->getId(), $path[count($path) - 1], "New Node ID should be the last node in the path");
  
      $found = $root->find($path);
      $this->assertNotNull($found, 'Node object should be found.');
      $this->assertEquals($node->getId(), $path[count($path) - 1], "Found node should have the correct ID");
      $this->assertEquals($node->getValue(), $value, "Should get the path value");
    }
 
  }

}
