<?php

namespace Alpha\Router\Routing;

use Alpha\Router\Routing\Store;

class Router
{
  private $store;

  private $loader = null;

  public function __construct()
  {
    $this->store = new Store();
  }

  /*
   * Helper Route registration methods.
   */
  public function get($url, $handler, $meta = [])
  {
    return $this->addRoute('get', $url, $handler, $meta);
  }
  public function put($url, $handler, $meta = [])
  {
    return $this->addRoute('put', $url, $handler, $meta);
  }
  public function post($url, $handler, $meta = [])
  {
    return $this->addRoute('post', $url, $handler, $meta);
  }
  public function delete($url, $handler, $meta = [])
  {
    return $this->addRoute('delete', $url, $handler, $meta);
  }

  public function nodeCount($pattern)
  {
    $trimmed = trim($pattern, '/');
    return empty($trimmed) ? 0 : count(explode('/', $trimmed));
  }

  public function parseUri($val)
  {
    //if the query string is in the URI
    $i    = strpos($val, '?');
    $path = $i > 0 ? substr($val, 0, $i) : $val;

    //trim off the trailing slashes.
    return $path == '/' ? $path : rtrim($path, '/');
  }

  public function addRoute($method, $url, $handler, array $meta = [])
  {

    $method = is_array($method) ? $method : [$method];

    // Clean the url.
    $url = $this->parseUri($url);

    foreach ($method as $verb) {
      $route = new Route($verb, $url, $handler, $meta);
      // Add the route data to the routingTable.
      $this->store->insert(Route::parse($verb) . $route->pattern(), $route);
    }
    return $this;
  }

  public function getRoute($method, $url): ?Route
  {
    // Clean The URL
    $url = $this->parseUri($url);

    $method = Route::parse($method);

    $route = $this->store->find($method . $url);

    /*
     * If the route is null or it does not exactly match the requested 
     * URL, attempt to find the fallback routes.
     */
    if (is_null($route) || !$route->isMatch($url)) {

      /*
       * Try to find if a fallback for the SPECIFIED method exits.
       */
      $route = $this->store->find($method . Route::parse('*'));

      /*
       * Try to find a catch all Route.
       */
      if (is_null($route)) {
        $route = $this->store->find(Route::parse('*') . Route::parse('*'));
      }
    }

    /*
     * If a Route object was found but it does not have a valid
     * handler, call the loader method if exists.
     * Its likely the route was unserialized from cache and needs
     * to be loaded from file.
     */

    if (!is_null($route) && !$route->hasHandler() && is_callable($this->loader)) {

      $fn = $this->loader;
      // Attempt the load the file using the supplied custom loader.
      $fn($route, $this);
      //Try to load the route again.
      return $this->getRoute($method, $url);
    }

    return $route;
  }

  public function export(callable $fn = null)
  {
    // Check if a custom export handler is supplied.
    if (!is_null($fn) && is_callable($fn)) {
    }

    return serialize($this->store);
  }

  /*
   *@method import - Unserialize the routing table string.
   *@param String $data Serialized routing table.
   *@param Callable $fn A function that is called when a route is being loaded from the cache.
   * Unserialized routes will not have the handler method present as resources are not serializable.
   * The callback is used to intercept the found Route object and perform transformations/Loading if
   * the handler is located in an external route file.
   *@return Object $this Return a reference to 'this'.
   */
  public function import($data, callable $fn)
  {
    $this->loader = $fn;

    $this->store = unserialize($data);
    return $this;
  }

  public function getRoutingTable()
  {
    return $this->store->getTree();
  }

  public function fallback(callable $handler, $meta = [])
  {
    return $this->addRoute('*', '*', $handler, $meta);
  }
}
