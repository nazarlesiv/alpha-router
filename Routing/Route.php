<?php
namespace Alpha\Router\Routing;

class Route implements \Serializable{

  static $parser = [];

  private $uri = null;
  private $method = null;
  private $pattern = null;
  private $handler = null;

  // Route parameter names.
  private $paramNames = null;

  // Route parameter values.
  private $paramValues = null;

  // Metadata.
  private $meta = [];

  // Set to true if all of the handlers were successfully called.
  private $resolved = false;


  static function setParser(callable $callback = null) {
    self::$parser[0] = $callback;
  }

  static function parse($url, &$matches = null) {
    /*
     * Check if a custom parser is supplied.
     */
    if((count(self::$parser) >= 1) && (is_callable(self::$parser[0]))) {
      return self::$parser[0]($url, $matches);
    }

    /* DEFAULT ROUTE PARSER */
    if(!is_string($url)) { return ''; }

    /*
     * The catchall url is signified by the '*' character.
     * This needs to be escaped for the regex pattern.
     */
    if($url === '*') {
      $url = '\*';
    }

    //If a matches is null create a new local array to capture the results.
    if(is_null($matches) || !is_array($matches)) {
      $matches = [];
    }

    return preg_replace_callback('/(:\w+)/',
      function($captured) use (&$matches) {
        $matches[] = $captured[ 0 ];
        return '(\\w+)';
      }, $url);
  }


  public function __construct($method = null, $uri = null, $handler = null, $meta = []) {
    $this->method($method);
    $this->uri($uri);
    if(!is_null($handler)) {
      $this->handler($handler);
    }
    $this->meta = is_array($meta) ? $meta : [];
  }

  public function method($val) {
    $this->method = $val;
    return $this;
  }
  public function getMethod() {
    return $this->method;
  }

  public function uri($val) {
    $this->uri = $val;

    return $this;
  }
  public function getUri() {
    return $this->uri;
  }

  public function serialize() {
    return serialize([
      'uri' => $this->uri,
      'method' => $this->method,
      'pattern'=> $this->pattern,
      'params'=> $this->paramNames,
      'meta'=> $this->meta
    ]);
  }

  public function unserialize($data) {
    $data = unserialize($data);
    foreach($data as $key => $value) {
      $this->$key = $value;
    }
  }

  public function handler($fn = null) {
    if(is_null($fn)) {
      throw new Exception('Invalid Handler function supplied');
    }
    // Check if is callable or an array of callables.

    $this->handler = is_array($fn) ? $fn : [$fn];
    return $this;
  }

  public function hasHandler(): bool {
    if(!is_null($this->handler) && is_array($this->handler) && count($this->handler) > 0) {
      return true;
    } 

    return false;
  }

  public function getHandler() {
    return $this->handler;
  }

  public function meta($key = null, $value = null) {
    if(is_null($key)) {
      return $this->meta;
    }
    if(is_null($value)) {
      return isset($this->meta[$key]) ? $this->meta[$key] : null;
    }
    $this->meta[$key] = $value;
    return $this;
  }

  public function pattern() {
    if(is_null($this->pattern)) {
      $this->process();
    }
    return $this->pattern;

  }
  public function paramNames() {
    if(is_null($this->paramNames)) {
      $this->process();
    }
    return $this->paramNames;
  }

  public function paramValues() {
    return $this->paramValues;
  }

  public function params() {
    return $this->buildParams($this->paramNames(), $this->paramValues());
  }

  public function resolve() {
    $handlers = $this->getHandler();
    $count = count($handlers);
    $args = [];

    for($i = 0; $i < $count; ++$i) {
      $fn = $handlers[$i];
      if(!is_callable($fn)) {
        throw new \Exception('Supplied handler is not callable.');
      }

      $args[] = $fn($this->params(), $args);
    }

    // Set the 'resolved' flag to true.
    $this->resolved = true;
    return $args;
  }

  public function resolved() {
    return $this->resolved;
  }

  // Convert the uri to regex and extract the params
  private function process() {
    $this->pattern = self::parse($this->uri, $this->paramNames);
    return $this;
  }

  //Compare the regest pattern with a string.
  public function isMatch($url) {
    //Match the pattern with the string and capture matched values.
    if(preg_match('#^' . $this->pattern() . '$#', $url, $values)) {
      $_url = array_shift($values);
      $this->paramValues = $values;
      return true;
    }
    return false;
  }

  public function nodes() {
    $trimmed = trim($this->uri, '/');
    return empty($trimmed) ? [] : explode('/', $trimmed);
  }

  public function export() {
    return [
      'method' => $this->getMethod(),
      'pattern' => $this->pattern(),
      'handler' => $this->getHandler(),
      'parameters' => $this->paramNames()
    ];
  }

  //combine the parameter names and values into an associative array to be passed to the handler.
  private function buildParams($pNames, $pValues) { 
    $params = [];
    $nCount = isset($pNames) ? count($pNames) : 0;
    $vCount = isset($pValues) ? count($pValues) : 0;

    if($nCount === $vCount && !is_null($pNames) &&
    !empty($pNames) && !empty($pValues)
    ) {
      for($i = 0; $i < $vCount; ++$i) {
        //Remove the initial ":" from the start of the string.
        $params[ ltrim($pNames[ $i ], ':') ] = $pValues[ $i ];
      }
    }
    return $params;
  }
}
