<?php
namespace Alpha\Router\Routing;

class Node
{

  // Node ID value
  private $id;

  // Node Stored Value
  private $value;

  // Child Nodes
  private $children;

  public function __construct($id)
  {
    $this->id = $id;
    $this->children = [];
  }

  public function isRoot()
  {
    return is_null($this->id);
  }

  public function create(array $path)
  {

    // If there are no more items in the $path, the current node is the value(leaf) node.
    if (count($path) === 0) {
      return $this;
    }

    // Extract the first item in the path
    $id = array_shift($path);

    /*
     * check if a child node exists with the current ID, 
     * if so, contiue traversal, otherwise create a new node and continue traversal.
     */
    $node = null;

    if (isset($this->children[$id])) {
      $node = $this->children[$id];
    } else {

      // Create a new node and store it as the child under the current ID
      $node = new Node($id);
      $this->children[$id] = $node;
    }
    return $node->create($path);
  }

  public function propagate($path)
  {
    /*
     * Create a copy of the path array for each child so that the array modifications 
     * arent 
     */
    $path = new \ArrayObject($path);

    /*
     * If children are indexed using the node values as a key, its possible to look at head into the 
     * children map using the next node in the list.
     * this would prevent the need to iterate over the children collection
     * NOTE: This will only work if each path node value is also used as a node ID
     */
    if ($path->count() > 0) {
      // Next node in the path
      $next = $path->offsetGet(0);

      // If exists, propagate the search on this node.
      if(isset($this->children[$next])) {
        return $this->children[$next]->find($path->getArrayCopy());
      }
      
    }

    /*
     * Iterate over the children collection and try to find a match
     */
    foreach ($this->children as $key => $value) {

      if (!is_null($match = $value->find($path->getArrayCopy()))) {
        return $match;
      }
    }
    return null;
  }

  public function find($path)
  {
    // If its a root node, iterate over the children and look for a match
    if ($this->isRoot()) {
      return $this->propagate($path);
    }

    // Extract the first item in the path
    $node = array_shift($path);

    // Match to see if the current node matches the path node.
    if (($this->id === $node) || (preg_match('#^' . $this->id . '$#', $node, $values))) {
      // print_r($values);

      /*
       *  If there are no more items left in the path, the current node is the match
       */
      if (count($path) === 0) {
        return $this;
      }

      /*
       *  Call find on all of the children of the current node 
       */
      return $this->propagate($path);
    }
    return null;
  }

  public function setValue($value)
  {
    $this->value = $value;
    return $this;
  }

  public function getValue()
  {
    return $this->value;
  }

  public function getId() {
    return $this->id;
  }
}