<?php
namespace Alpha\Router\Routing;
use Alpha\Router\Routing\Node;

class Store
{
  private $root;

  private $tokenizer = null;

  public function setTokenizer(callable $fn)
  {
    if (!is_callable($fn)) {
      throw new \Exception("Invalid Tokenizer function supplied.");
    }
    $this->tokenizer = $fn;
  }

  public function getTokenizer(): ?callable
  {
    return $this->tokenizer;
  }

  public function tokenize($path): array
  {
    if (!is_null($tokenizer = $this->getTokenizer())) {
      return $tokenizer($path);
    }
    return explode('/', trim($path, '/'));
  }

  public function __construct()
  {
    $this->root = new Node(null, null);
  }

  public function insert($location, $value)
  {
    $path = $this->tokenize($location);
    $node = $this->root->create($path);

    $node->setValue($value);
    return $this;
  }

  public function find($location)
  {
    $path = $this->tokenize($location);
    $matches = [];
    $node = $this->root->find($path, $matches);
    return !is_null($node) ? $node->getValue() : null;
  }

  public function getTree()
  {
    return $this->root;
  }
}
