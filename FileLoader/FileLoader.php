<?php
namespace Alpha\Router\FileLoader;

class FileLoader {

  // Default route file pattern used to match files.
  private $_pattern = '/(.*)route\.php$/i';

  // Matched files.
  private $_matches = [];

  // Loaded files.
  private $_loaded = [];

  // Errors Array
  private $_error = [];


  public function __construct($args = null) {

  }

  /*
    @method reduce - recursively checks all files in the given path and saves the matches.
    @param Mixed $path - a String or Array of strings representing the root locations to search.
    @param String $pattern - A regex patters against which to match each file.
    @param callable $cb - a Callback function to be called on each match.
   */
  public function match($path, $pattern = null, callable $cb = null) {

    // Check if a pattern was supplied, if not default to the instance property.
    $pattern = !is_null($pattern) ? $pattern : $this->_pattern;

    // Check if a pattern is a valid string.
    if(empty($pattern) || !is_string($pattern)) {
      throw new Exception("Invalid pattern Supplied");
      return;
    }

    // Make sure the list of directories is an array.
    $directories = is_array($path) ? $path : [$path];
    $count = count($directories);

    for($i = 0; $i < $count; ++$i) {

      $dir = $directories[$i];
      // Check the directory is a valid string.
      if(!is_string($dir) || !is_dir($dir)) { continue; }

      if($handle = opendir($dir)) {

        while(false !== ($entry = readdir($handle))) {

          // Validate the entry is not in the exluded path list.
          if($entry === '.' || $entry === '..') { continue; }

          //Build the full file path.
          $filepath = rtrim($dir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $entry;

          // If entry is another directory, recursively call loadFiles.
          if(is_dir($filepath)) {
            $this->match($filepath, $pattern, $cb);
            // Dont bother proceeding since this is a directory.
            continue;
          }

          // Test the entry against the matching regex.
          if(!preg_match($pattern, $entry)) { continue; }

          if(is_file($filepath)) {
            $this->_matches[] = $filepath;

            // If an onMatch callback is supplied, call it.
            if(!is_null($cb) && is_callable($cb)) {
              $cb($filepath);
            }
          }
        }

        // Close the directory handler.
        closedir($handle);
      }
    }

    // Return the matched filepaths.
    return $this->_matches;
  }

  // Load the contents of a file and call the supplied callback or return it.
  public function load($filepath) {
    /*
     * Check if the the file is already loaded.
     * this is to prevent infinite recursion if the 'route'
     * method is called from within a route file.
     * And accidentaly loading duplicate files.
     */
    if(isset($this->_loaded[$filepath])) {
      $this->_error[] = 'Attempting to load a duplicate file: "' . $filepath . '"';
      return false;
    }

    //Get the file info and check the extension.
    $info = pathinfo($filepath);

    if($info && isset($info[ 'extension' ]) && strtolower($info[ 'extension' ]) === 'php') {

      //Load the route file.
      $route = require($filepath);

      // Add the filepath to the loadedRouteFile registry.
      $this->_loaded[$filepath] = $route;

      return $route;
    }
    return null;
  }
}
