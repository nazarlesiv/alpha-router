<?php
namespace Alpha\Router\Middleware;

class Middleware {
  //middlewear handlers
  private $_middleware = [];

  private $parser = null;

  public function __construct($config = null) {

    // Defalt Parcer is set durring construction.
    $defaultParser = (function ($url) {
      if(!is_string($url)) {
        return '';
      }
      return preg_replace('/(:\w+)/','(\\w+)', $url);
    });

    $this->setDefaultParcer($defaultParser);

  }

  public function setDefaultParcer(callable $callback = null) {
    $this->parser = $callback;
  }

  public function toRegex($url) {
    $fn = $this->parser;
    return $fn($url);
  }

  public function nodeCount($pattern) {
    $trimmed = trim($pattern, '/');
    return empty($trimmed) ? 0 : count(explode('/', $trimmed));
  }

  public function count() {
    return count($this->_middleware);
  }

  //Mount a middleware method at the specified mount point.
  //If the mount point is not specified, set it to root, which will always call the handler.
  public function add($pattern, callable $handler = null) {
    $path = null;
    $fn   = null;

    //If Only 1 parameter is supplied, and it is a callable function, set it to the root mount point.
    if(is_callable($pattern) && is_null($handler)) {
      $path = '/';
      $fn   = $pattern;
    } else if(is_string($pattern) && is_callable($handler)) {
      $path = $this->toRegex($pattern);
      $fn   = $handler;
    } else {
      throw new \Exception('Invalid argument supplied to: ' . __METHOD__);

      return;
    }

    // Split the mount path string on the / and count the nodes.
    $count = $this->nodeCount($path);
    if(!isset($this->_middleware[$count])) {
      $this->_middleware[$count] = [];
    }

    if(!isset($this->_middleware[$count][$path])) {
      $this->_middleware[$count][$path] = [$fn];
    } else {
      $this->_middleware[$count][$path][] = $fn;
    }
    return $this;
    
  }

  /*
   * @reduceMiddleware($uri) - Reduce the list of middleware using a regest pattern.
   */
  public function reduce($query = '') {
    $collection = [];
    /*
     * If no pattern is supplied do not return results.
     * a root mount point of '/' should pass this validation
     * and be parsed to a count of 0 nodes, which would return
     * all middleware.
     */

    if(empty($this->_middleware) || !is_scalar($query)) {
      return $collection;
    }

    // If the pattern is empty, return all middleware.
    if(empty($query)) {
      foreach($this->_middleware as $count) {
        foreach($count as $uri => $fn) {
          $ct = count($fn);
          for($i = 0; $i < $ct; ++$i) {
            $collection[] = $fn[$i];
          }
        }
      }
      return $collection;
    }

    // Split the pattern and count the nodes.
    $nodeCount = $this->nodeCount($query);

    //print_r(array_keys($this->_middleware));
    for($k = 0; $k <= $nodeCount; ++$k) {
      // If No middleware exists for the current count, continue to the next.
      if(!isset($this->_middleware[$k]) || empty($this->_middleware[$k])) {
        continue;
      }

      foreach($this->_middleware[$k] as $key => $fn) {
        //print_r($key);
        //Match the key to the _matchedUrl.

        if($this->leftMatch($key, $query)) {
          $count = count($fn);
          for($i = 0; $i < $count; ++$i) {
            $collection[] = $fn[ $i ];
          }
        }
      }
    }
    return $collection;
  }

  public function leftMatch($pattern, $string) {
    if(preg_match('#^' . $pattern . '#', $string, $values)) {
      return $values;
    }
    return false;
  }
}
